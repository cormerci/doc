** 3D Visualization with VirtualGL and TurboVNC
:PROPERTIES:
:CUSTOM_ID: visu
:END:

- Install and setup on your desktop [[https://sourceforge.net/projects/turbovnc/files/][TurboVNC Viewer]]
- Connect to plafrim :

  #+begin_src sh :eval never-export
  $ module load slurm visu/srun
  $ srun-visu
  #+end_src

- *The first time, TurboVNC will ask you for a password to secure the
  X11 session*

- Wait for a result like :

  #+begin_src sh :eval never-export
  Waiting for a slot on a visualization serverUsing 3D visualization
  with VirtualGL and TurboVNC

  Desktop 'TurboVNC: visu01:1 (login)' started on display visu01:1

  Starting applications specified in /home/login/.vnc/xstartup.turbovnc

  Log file is /home/login/.vnc/visu01:1.log

  Launched vncserver: visu01:1

  Now, in another terminal, open a new SSH session to plafrim like this: "ssh plafrim -N -L 5901:visu01:5901 &amp;" and launch TurboVNC viewer (vncviewer command) on your desktop on "localhost:1"
  #+end_src


- Now open another SSH using the suggested command.

- In the *Applications* menu, you will find the *Visit* and *Paraview* softwares.

- In order to use a 3D (OpenGL) program via the CLI, put *vglrun* before the command, like: ~vglrun paraview~

- Halting ~srun-visu~ (via Ctrl-C ou scancel) or closing the first SSH session will stop the post-processing session.

- On you desktop, run [[https://cdn.rawgit.com/TurboVNC/turbovnc/2.1.1/doc/index.html][TurboVNC vncviewer]]

- vncviewer will ask you for the DISPLAY value (from previous command) and the session password.

- *Important* : The default session time is limited to *two hours*. To specify a different session time, use :

  #+begin_src sh :eval never-export
  $ srun-visu --time=HH:MM:SS
  #+end_src

- A session time cannot exceed *eight hours* (~--time=08:00:00~)
