import os
import re
import subprocess
from datetime import datetime

# Creating the full guix reference as a file
stdout_fname = 'guix_rev.txt'
with open(stdout_fname, 'w+') as f:
    subprocess.call(['guix', 'describe'], stdout=f, stderr=subprocess.STDOUT)
    f.seek(0)
    guix = f.read()

res = re.findall('^  ([\w-]+) ([\w]+)', guix, re.MULTILINE)
fname = 'guix-rev'
for p in res:
    fname += '#'+p[0]+'_'+p[1]

to_keep = os.path.join(os.environ['HOME'], fname)
if not os.path.exists(to_keep):
    os.rename(stdout_fname, to_keep)
else:
    os.remove(stdout_fname)

# Adding one line in the reproducibility logs

# Warning, the CI_JOB_ID environment variable is defined in the shell created
# by the gitlab runner. It has to be passed to SLURM (sbatch --export=ALL)
# and/or to guix (--preserve=^CI_) if this script is executed within a batch
# script and/or a guix environment
job_id = os.environ['CI_JOB_ID']
now = datetime.now().strftime("%d/%m/%Y %H:%M:%S")

logs = os.path.join(os.environ['HOME'], 'reproducibility_logs.txt')
if not os.path.exists(logs):
    with open(logs, 'w') as f:
        f.write('#  Date     Time    job_id         guix_id\n')

with open(logs, 'a') as f:
    f.write(' '.join([now, job_id, fname, '\n']))
